queryString = require("querystring")

class Paginator

  constructor: (req, res, options = {}) ->
    @req = req
    @res = res

    # Setup paging
    page = parseInt(req.param('page') || req.get('pagination.page'))
    @page = if page > 0 then page else 1

    perPage = parseInt(req.param('perPage') || req.get('pagination.perPage'))
    @perPage = if perPage > 0 then perPage else options.perPage || 50
    @total = 0

    # Initialize headers
    @setHeaders()


  setTotal: (total) ->
    @total = total
    @setHeaders()


  #
  # Helpers
  #
  pageOffset: ->
    (@page - 1) * @perPage

  nextPageOffset: ->
    @page * @perPage

  hasPrevPage: ->
    @page > 1

  hasNextPage: ->
    @nextPageOffset() < @total

  ###
    Add pagination headers

    Renders JSON or HTML based on request content-type headers
  ###
  setHeaders: ->
    # Set pagination headers
    @res.setHeader 'pagination.page', @page
    @res.setHeader 'pagination.perPage', @perPage
    @res.setHeader 'pagination.total', @total

    if rslt = /(.*)\?.*$/.exec(@req.originalUrl)
      baseUrl = rslt[1]

    baseUrl ||= @req.originalUrl

    # Set link headers
    links = {}
    @res.set('link', '')

    # next link
    if @hasNextPage()
      nextParams =
        page: @page + 1
        perPage: @perPage
      for key, value of @req.query
        nextParams[key] ||= value
      links.next = baseUrl + "?" + queryString.stringify(nextParams)

    # prev link
    if @hasPrevPage()
      prevParams =
        page: @page - 1
        perPage: @perPage
      for key, value of @req.query
        prevParams[key] ||= value
      links.prev = baseUrl + "?" + queryString.stringify(prevParams)

    @res.links links


module.exports = Paginator