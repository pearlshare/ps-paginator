# Paginator

Pagination tool to set pagination and link headers using express.

## Features

Will automatically parse the page from a query option `?page=3` or req headers `pagination.page: 3` and the perPage settings `?perPage=10` or `pagination.perPage: 10`.

The response headers will be set with pagination headers and link headers.

request: `GET /search?page=3&PerPage=10`

response:
  headers:
    pagination.page: 3
    pagination.perPage: 10
    links: </search?page=4&perPage=10>; rel="next", </search?page=2&perPage=10>; rel="prev"

## Usage

```coffee
  Paginator = require("ps-paginator")

  app.use (req, res, next) ->
    res.paginator = new Paginator(req, res, perPage: 10, )
    next()

  app.get 'items', (req, res) ->
    Item.find()
      .limit(req.paginator.perPage)
      .offset(res.paginator.pageOffset())
      .exec (err, items, count) ->
        res.paginator.setTotal(count)
        res.json(items)

```

### License

MIT