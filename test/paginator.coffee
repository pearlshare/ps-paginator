chai = require('chai')
expect = chai.expect

Paginator = require('../index')

class Req
  constructor: (opts = {}) ->
    @originalUrl = opts.originalUrl || '/search'
    @query = opts.query || {}
    @headers = opts.headers || {}

  param: (key) ->
    @query[key]

  get: (key) ->
    @headers[key]

class Res
  constructor: (opts = {}) ->
    @headers = opts

  setHeader: (key, value) ->
    @headers[key] = value

  links: (links) ->
    linkList = []
    Object.keys(links).forEach (rel) ->
      url = links[rel]
      linkList.push "<#{url}>; rel=\"#{rel}\""

    @headers.links = linkList.join(', ')

describe 'paginator', ->

  it 'should pull out the page query', () ->
    req = new Req(query: {page: 1, perPage: 10})
    res = new Res

    paginator = new Paginator(req, res)

    expect(res.headers['pagination.page']).to.equal(1)
    expect(res.headers['pagination.perPage']).to.equal(10)

  it 'should set the link headers', () ->
    req = new Req(query: {page: 2, perPage: 10})
    res = new Res

    paginator = new Paginator(req, res)
    paginator.setTotal(100)

    expect(res.headers.links).to.equal('</search?page=3&perPage=10>; rel="next", </search?page=1&perPage=10>; rel="prev"')
    expect(res.headers['pagination.total']).to.equal(100)
